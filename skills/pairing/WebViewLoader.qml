import QtQuick 2.4
import QtWebView 1.1


WebView {
    id: webview
    url: "https://home.mycroft.ai"
    anchors.fill: parent
}
